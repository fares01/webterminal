from permission.models import Permission as authPermission
from common.models import AssignFilterToUser, Notification, TimeSlot
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from common.models import Log, CommandLog
import re
import socket
from channels.generic.websockets import WebsocketConsumer
from channels import Group
try:
    import simplejson as json
except ImportError:
    import json
import datetime

def sendNotification(username=None, ip=None, credential=None, commande=None):
    from webterminal.asgi import channel_layer
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    data = AssignFilterToUser.objects.all()
    notif = Notification(description=f"alerte user: {username}, ip: {ip}, credential: {credential} "
                                     f"commande used {commande}", user=username)
    notif.seen = False
    description = f"alerte user: {username}, ip: {ip}, credential: {credential} commande used {commande}"
    data_notif = {
        'description': description
    }
    data_notif['date'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    #Group("nautif").send({"text": description})
    def myconverter(o):
        if isinstance(o, datetime.datetime):
            return o.__str__()
    Group("nautif").send({"text": json.dumps(data_notif, default = myconverter) })
    notif.save()

def search(username=None, ip=None, credential=None, cmd_str=None, logobj=None):
    # from webterminal.asgi import channel_layer
    # Group("nautif").send({
    #     "text": "gilo",
    # })
    if(len(cmd_str) != 0):

        resultatfinale =''
        pattern = re.compile(']0')
        pattern2 = re.compile(r':~# ')
        pattern3 = re.compile(r':~\$ ')

        resultat = pattern.findall(cmd_str)
        resultat2 = pattern2.findall(cmd_str)
        resultat3 = pattern3.findall(cmd_str)

        if (resultat):
            if(resultat2):
                resultatfinale = cmd_str.split('# ')[-1]
            elif(resultat3):
                resultatfinale = cmd_str.split('$ ')[-1]
                toLog(logobj, resultatfinale)
                ifExist(username,ip , credential, resultatfinale)
        elif(resultat2):
            resultatfinale = cmd_str.split('# ')[-1]
            toLog(logobj, resultatfinale)
            ifExist(username, ip, credential, resultatfinale)
        elif(resultat3):
            resultatfinale = cmd_str.split('$ ')[-1]
            toLog(logobj, resultatfinale)
            ifExist(username, ip, credential, resultatfinale)


def search2(username=None, ip=None, credential=None, cmd_str=None, logobj=None):
    if(len(cmd_str) != 0):
        resultatfinale =''

        pattern = re.compile(']0')
        pattern2 = re.compile(r':~# ')
        pattern3 = re.compile(r':~\$ ')

        resultat = pattern.findall(cmd_str)
        resultat2 = pattern2.findall(cmd_str)
        resultat3 = pattern3.findall(cmd_str)
        if (resultat):
            if(resultat2):
                resultatfinale = cmd_str.split('# ')[-1]
            elif(resultat3):
                resultatfinale = cmd_str.split('$ ')[-1]
                CommandLog.objects.create(log=logobj, command=resultatfinale)
                ifExist(username,ip , credential, resultatfinale)

        elif(resultat2):
            resultatfinale = cmd_str.split('# ')[-1]
            CommandLog.objects.create(log=logobj, command=resultatfinale)
            ifExist(username, ip, credential, resultatfinale)
        elif(resultat3):
            resultatfinale = cmd_str.split('$ ')[-1]

            CommandLog.objects.create(log=logobj, command=resultatfinale)
            ifExist(username, ip, credential, resultatfinale)
        elif (cmd_str):
            i=0
            cmd = ''
            CommandLog.objects.create(log=logobj, command=cmd_str)
            ifExist(username, ip, credential, cmd_str)


def ifExist(username=None, ip=None, credential=None, commande=None):

    user = username
    userdata = User.objects.filter(username__exact=user).get()
    filterdata = AssignFilterToUser.objects.filter(user__exact=userdata)
    for e in filterdata:
        filtercommands = e.filter.commands

        list = subString(filtercommands)
        if commande in list:

            sendNotification(username , ip, credential, commande)
            break
        else:
            for e in list:

                if(e in commande):
                    sendNotification(username, ip, credential, e)
                    break
                else:
                 pass




def subString(commande):
    s = commande
    start = s.find('["') + len('["')
    end = s.find('"]')
    substring = s[start:end]
    list = substring.split('", "')
    return list


def sendClose(userdata, ip, credential):
    user = userdata
    userdata = User.objects.filter(username__exact=user).get()
    timeslot = TimeSlot.objects.filter(user__exact=userdata)
    if(timeslot):
        for c in timeslot:
            start = c.start_at
            end = c.end_at
            close = c.close
        now = datetime.datetime.now()
        data_notif = {'toclose': close,'ip': ip}
        if(now > start and now <end):
            pass
        else:
            Group("close").send({"text": json.dumps(data_notif)})
            permission = Permission.objects.get(codename='can_connect_serverinfo')
            authperm = authPermission.objects.filter(user=userdata).get()
            authperm.permissions.remove(permission)
            userdata.user_permissions.remove(permission)

def toLog(logobj , commande):
    if  "" in commande:
        pass
    else: CommandLog.objects.create(log=logobj, command=commande)



class terminal(WebsocketConsumer):
    def connect(self, message):
        self.message.reply_channel.send({"accept": True})
        Group("nautif").add(message.reply_channel)

    def disconnect(self, message,**kwargs):
        Group("nautif").discard(message.reply_channel)

    # close threading
    @property
    def queue(self):
        queue = get_redis_instance()
        channel = queue.pubsub()
        return queue
    def receive(self, text=None, bytes=None, **kwargs):
        #print('ici cest mesagge sa7bi' , text)
        #self.send("message: message")
        """text_data_json = json.loads(text_data)
        message = text_data_json['message']
        print(message)
        self.send(text_data=json.dumps({
            'message': message
        }))"""


class closeTerminal(WebsocketConsumer):
    def connect(self, message):
        self.message.reply_channel.send({"accept": True})
        Group("close").add(message.reply_channel)

    def disconnect(self, message,**kwargs):
        Group("close").discard(message.reply_channel)

    # close threading
    @property
    def queue(self):
        queue = get_redis_instance()
        channel = queue.pubsub()
        return queue
    def receive(self, text=None, bytes=None, **kwargs):
        #print('ici cest mesagge sa7bi' , text)
        #self.send("message: message")
        """text_data_json = json.loads(text_data)
        message = text_data_json['message']
        print(message)
        self.send(text_data=json.dumps({
            'message': message
        }))"""

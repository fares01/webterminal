from rest_framework import viewsets
from common.serializers import ServerGroupSerializer, ServerInforSerializer, CommandsSequenceSerializer, \
    CredentialSerializer
from common.models import ServerGroup, ServerInfor, CommandsSequence, Credential
from .models import AssignFilterToUser, FilterCommands
from .serializers import AssignFilterToUserSerializer, FilterCommandsSerializer, UserSerializer
from django.contrib.auth.models import User


class ServerGroupViewSet(viewsets.ModelViewSet):
    queryset = ServerGroup.objects.all()
    serializer_class = ServerGroupSerializer


class ServerInforViewSet(viewsets.ModelViewSet):
    queryset = ServerInfor.objects.all()
    serializer_class = ServerInforSerializer


class CredentialViewSet(viewsets.ModelViewSet):
    queryset = Credential.objects.all()
    serializer_class = CredentialSerializer


class CommandsSequenceViewSet(viewsets.ModelViewSet):
    queryset = CommandsSequence.objects.all()
    serializer_class = CommandsSequenceSerializer


# ----------------------------------------
#     SSH filter
# ----------------------------------------
class AssignFilterViewSet(viewsets.ModelViewSet):
    queryset = AssignFilterToUser.objects.all()
    serializer_class = AssignFilterToUserSerializer


class FilterCommandsViewSet(viewsets.ModelViewSet):
    queryset = FilterCommands.objects.all()
    serializer_class = FilterCommandsSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

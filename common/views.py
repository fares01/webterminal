# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.mixins import AccessMixin
from django.utils.translation import activate
from django.views.generic import View
from django.http import JsonResponse
from common.models import ServerGroup, CommandsSequence, Credential, ServerInfor, Log, CommandLog, Notification, TimeSlot
try:
    import simplejson as json
except ImportError:
    import json
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.utils.encoding import smart_str
from django.views.generic.list import ListView
from django.core.serializers import serialize
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import TemplateView, FormView
from django.core.exceptions import PermissionDenied
import traceback
from django.contrib.auth.views import redirect_to_login
from django.utils.translation import ugettext_lazy as _
import pytz
import uuid
from common.utils import get_redis_instance, get_settings_value
from common.extra_views import PasswordResetView as PasswordResetViewNew, PasswordResetDoneView as PasswordResetDoneViewNew, PasswordResetConfirmAndLoginView
from common.forms import PasswordResetForm, SetPasswordForm, SettingsForm
from django.urls import reverse_lazy
from django.conf import settings
from django.contrib import messages
from django_otp.plugins.otp_hotp.models import HOTPDevice
from django_otp.plugins.otp_totp.models import TOTPDevice
from .models import FilterCommands, AssignFilterToUser, TimeSlot
from django.contrib.auth.models import User
import re
from datetime import datetime, timedelta
from webterminal.encrypttion_util import encrypt, decrypt
__webterminalhelperversion__ = '0.3'


class LoginRequiredMixin(AccessMixin):
    """
    CBV mixin which verifies that the current user is authenticated.
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        activate(request.LANGUAGE_CODE.replace('-', '_'))
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)

    def handle_no_permission(self):
        if self.raise_exception and self.request.user.is_authenticated:
            raise PermissionDenied(self.get_permission_denied_message())
        return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())


class Commands(LoginRequiredMixin, TemplateView):
    template_name = 'common/commandcreate.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if not request.user.is_authenticated:
            return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if not request.user.has_perm('common.can_add_commandssequence'):
            raise PermissionDenied(_('403 Forbidden'))
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(Commands, self).get_context_data(**kwargs)
        context['server_groups'] = ServerGroup.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context

    def post(self, request):
        if request.is_ajax():
            try:
                if isinstance(request.body, bytes):
                    data = json.loads(request.body.decode())
                else:
                    data = json.loads(request.body)
                if data['action'] == 'create':
                    if not request.user.has_perm('common.can_add_commandssequence'):
                        raise PermissionDenied(_('403 Forbidden'))
                    obj = CommandsSequence.objects.create(
                        name=data['name'], commands=data['commands'])
                    for group in data['group']:
                        obj.group.add(ServerGroup.objects.get(name=group))
                    obj.save()
                    return JsonResponse({'status': True, 'message': '%s create success!' % (smart_str(data.get('name', None)))})
                elif data['action'] == 'update':
                    if not request.user.has_perm('common.can_change_commandssequence'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = CommandsSequence.objects.get(
                            id=data.get('id', None))
                        obj.commands = data['commands']
                        [obj.group.remove(group)
                         for group in obj.group.all()]
                        for group in data['group']:
                            obj.group.add(
                                ServerGroup.objects.get(name=group))
                        data.pop('group')
                        obj.__dict__.update(data)
                        obj.save()
                        return JsonResponse({'status': True, 'message': '%s update success!' % (smart_str(data.get('name', None)))})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                elif data['action'] == 'delete':
                    if not request.user.has_perm('common.can_delete_commandssequence'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = CommandsSequence.objects.get(
                            id=data.get('id', None))
                        taskname = obj.name
                        obj.delete()
                        return JsonResponse({'status': True, 'message': 'Delete task %s success!' % (taskname)})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                else:
                    return JsonResponse({'status': False, 'message': 'Illegal action.'})
            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Please input a valid group name!'})
            except IntegrityError:
                return JsonResponse({'status': False, 'message': 'Task name:%s already exist,Please use another name instead!' % (data['name'])})
            except KeyError:
                return JsonResponse({'status': False, 'message': "Invalid parameter,Please report it to the adminstrator!"})
            except Exception as e:
                print(traceback.print_exc())
                return JsonResponse({'status': False, 'message': 'Some error happend! Please report it to the adminstrator! Error info:%s' % (smart_str(e))})
        else:
            pass


class CommandExecuteList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = CommandsSequence
    template_name = 'common/commandslist.html'
    permission_required = 'common.can_view_commandssequence'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(CommandExecuteList, self).get_context_data(**kwargs)
        context['server_groups'] = ServerGroup.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class CommandExecuteDetailApi(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'common.can_execute_commandssequence'
    raise_exception = True

    def post(self, request):
        if request.is_ajax():
            id = request.POST.get('id', None)
            try:
                data = CommandsSequence.objects.get(id=id)
                return JsonResponse({'status': True, 'name': data.name, 'commands': json.loads(data.commands), 'data': {'name': data.name, 'commands': json.loads(data.commands), 'group': [group.name for group in data.group.all()]}})
            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Request object not exist!'})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})


class CredentialCreate(LoginRequiredMixin, TemplateView):
    template_name = 'common/credentialcreate.html'

    def get_context_data(self, **kwargs):
        context = super(CredentialCreate, self).get_context_data(**kwargs)
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context

    def post(self, request):
        if request.is_ajax():
            try:
                if isinstance(request.body, bytes):
                    data = json.loads(request.body.decode())
                else:
                    data = json.loads(request.body)
                id = data.get('id', None)
                action = data.get('action', None)
                password = data.get('password', None)
                passwordssh = encrypt(password)
                password_decrypt = decrypt(passwordssh)
                fields = [
                    field.name for field in Credential._meta.get_fields()]
                data = {k: v for k, v in data.items() if k in fields}
                if action == 'create':
                    if not request.user.has_perm('common.can_add_credential'):
                        raise PermissionDenied(_('403 Forbidden'))
                    obj = Credential.objects.create(**data)
                    obj.password = passwordssh
                    obj.save()
                    return JsonResponse({'status': True, 'message': 'Credential %s was created!' % (obj.name)})
                elif action == 'update':
                    if not request.user.has_perm('common.can_change_credential'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = Credential.objects.get(id=id)
                        obj.__dict__.update(**data)
                        obj.password = passwordssh
                        obj.save()
                        return JsonResponse({'status': True, 'message': 'Credential %s update success!' % (smart_str(data.get('name', None)))})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                elif action == 'delete':
                    if not request.user.has_perm('common.can_delete_credential'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = Credential.objects.get(id=id)
                        obj.delete()
                        return JsonResponse({'status': True, 'message': 'Delete credential %s success!' % (smart_str(data.get('name', None)))})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                else:
                    return JsonResponse({'status': False, 'message': 'Illegal action.'})
            except IntegrityError:
                return JsonResponse({'status': False, 'message': 'Credential %s already exist! Please use another name instead!' % (smart_str(json.loads(request.body).get('name', None)))})
            except Exception as e:
                print(traceback.print_exc())
                return JsonResponse({'status': False, 'message': 'Error happend! Please report it to adminstrator! Error:%s' % (smart_str(e))})


class CredentialList(LoginRequiredMixin, PermissionRequiredMixin, ListView):

    model = Credential
    template_name = 'common/credentiallist.html'
    permission_required = 'common.can_view_credential'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(CredentialList, self).get_context_data(**kwargs)
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class CredentialDetailApi(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'common.can_view_credential'
    raise_exception = True

    def post(self, request):
        if request.is_ajax():
            id = request.POST.get('id', None)
            data = Credential.objects.filter(id=id)
            for c in data:
                password = c.password
                password_decrypt = decrypt(password)
                c.password = password_decrypt

            if data.count() == 0:
                return JsonResponse({'status': False, 'message': 'Request object not exist!'})
            return JsonResponse({'status': True, 'message': json.loads(serialize('json', data))[0]['fields']})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})


class ServerCreate(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'common/servercreate.html'
    permission_required = 'common.can_add_serverinfo'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(ServerCreate, self).get_context_data(**kwargs)
        context['credentials'] = Credential.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class ServerlList(LoginRequiredMixin, PermissionRequiredMixin, ListView):

    model = ServerInfor
    template_name = 'common/serverlist.html'
    permission_required = 'common.can_view_serverinfo'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(ServerlList, self).get_context_data(**kwargs)
        context['credentials'] = Credential.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class GroupList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = ServerGroup
    template_name = 'common/grouplist.html'
    permission_required = 'common.can_view_servergroup'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(GroupList, self).get_context_data(**kwargs)
        context['servers'] = ServerInfor.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class GroupCreate(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'common/groupcreate.html'
    permission_required = 'common.can_add_servergroup'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(GroupCreate, self).get_context_data(**kwargs)
        context['servers'] = ServerInfor.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class LogList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Log
    template_name = 'common/logslist.html'
    permission_required = 'common.can_view_log'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(LogList, self).get_context_data(**kwargs)
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class CommandLogList(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'common.can_view_command_log'
    raise_exception = True

    def post(self, request):
        if request.is_ajax():
            id = request.POST.get('id', None)
            data = CommandLog.objects.filter(log__id=id)
            if data.count() == 0:
                return JsonResponse({'status': False, 'message': 'Request object not exist!'})
            return JsonResponse({'status': True, 'message': [{'datetime': i.datetime.astimezone(pytz.timezone(getattr(settings, "TIME_ZONE", 'UTC'))).strftime('%Y-%m-%d %H:%M:%S'), 'command': i.command} for i in data]})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})


class WebterminalHelperDetectApi(LoginRequiredMixin, View):

    def post(self, request):
        if request.is_ajax():
            conn = get_redis_instance()
            version = request.POST.get('version', None)
            protocol = request.POST.get('protocol', None)
            identify = request.POST.get('identify', None)
            if identify is not None and version is None and protocol is None:
                # get identify id
                id = str(uuid.uuid4())
                conn.set(id, 'ok')
                conn.expire(id, 15)
                return JsonResponse({'status': True, 'message': id})
            elif protocol in ["rdp", "ssh", "sftp"] and identify:
                identify_data = conn.get(identify)
                if isinstance(identify_data, bytes):
                    identify_data = identify_data.decode()
                else:
                    identify_data = identify_data
                if identify_data == 'installed':
                    conn.delete(identify)
                    return JsonResponse({'status': True, 'message': 'ok'})
                elif identify_data == 'need upgrade':
                    conn.delete(identify)
                    return JsonResponse({'status': False, 'message': 'Webterminal helper need upgrade to version: {0}'.format(__webterminalhelperversion__)})
                else:
                    conn.delete(identify)
                    # not install webterminal helper
                    return JsonResponse({'status': False, 'message': 'no'})
            else:
                return JsonResponse({'status': False, 'message': 'Method not allowed!'})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})


class WebterminalHelperDetectCallbackApi(View):

    def post(self, request):
        if not request.is_ajax():
            conn = get_redis_instance()
            version = request.POST.get('version', None)
            protocol = request.POST.get('protocol', None)
            identify = request.POST.get('identify', None)
            if version and protocol in ["rdp", "ssh", "sftp"] and identify:
                identify_data = conn.get(identify)
                if isinstance(identify_data, bytes):
                    identify_data = identify_data.decode()
                else:
                    identify_data = identify_data
                if identify_data == 'ok' and version == __webterminalhelperversion__:
                    conn.set(identify, 'installed')
                    return JsonResponse({'status': True, 'message': 'ok'})
                elif identify_data == 'ok' and version != __webterminalhelperversion__:
                    conn.set(identify, 'need upgrade')
                    return JsonResponse({'status': False, 'message': 'no'})
                else:
                    conn.delete(identify)
                    # not install webterminal helper
                    return JsonResponse({'status': False, 'message': 'no'})
            else:
                return JsonResponse({'status': False, 'message': 'Method not allowed!'})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})


class PasswordResetView(PasswordResetViewNew):
    form_class = PasswordResetForm
    template_name = "common/password-reset.html"
    success_url = reverse_lazy("password-reset-done")
    subject_template_name = "common/emails/password-reset-subject.txt"
    email_template_name = "common/emails/password-reset-email.html"


class PasswordResetDoneView(PasswordResetDoneViewNew):
    template_name = "common/password-reset-done.html"


class PasswordResetConfirmView(PasswordResetConfirmAndLoginView):
    success_url = reverse_lazy('index')
    template_name = "common/password-reset-confirm.html"
    form_class = SetPasswordForm


class SettingsView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    success_url = reverse_lazy('settings')
    template_name = "common/settings.html"
    form_class = SettingsForm
    permission_required = 'common.can_change_settings'
    raise_exception = True

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.set_settings(self.request)
        return super(SettingsView, self).form_valid(form)

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view. To avoid form initial cache.
        """
        initial = super().get_initial()
        initial['webterminal_detect'] = get_settings_value(
            "detect_webterminal_helper_is_installed")
        initial['otp_switch'] = get_settings_value("otp")
        initial['timezone'] = getattr(settings, "TIME_ZONE", 'UTC')
        initial['use_tz'] = getattr(settings, "USE_TZ", True)
        return initial

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['settings'] = True
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class SettingsOtpView(LoginRequiredMixin, TemplateView):
    template_name = "common/settings.html"

    def get_context_data(self, **kwargs):
        context = super(SettingsOtpView, self).get_context_data(**kwargs)
        context['settings'] = False
        context['otp'] = True
        if not get_settings_value("otp"):
            context['otp'] = False
            messages.add_message(self.request, messages.ERROR, _(
                'You must set otp switch to open then use this function!'))
        if get_settings_value("otp"):
            obj, created = TOTPDevice.objects.update_or_create(
                user=self.request.user, defaults={"name": "webterminal"})
            if created:
                messages.add_message(self.request, messages.ERROR, _(
                'You must configure otp or you can not login to this system again!'))
            context["qrcode_url"] = reverse_lazy(
                "admin:otp_totp_totpdevice_qrcode", kwargs={"pk": obj.pk})
            context["config_url"] = obj.config_url
            first = datetime.today() - timedelta(days=7)
            last = datetime.today() + timedelta(days=1)
            first_f = first.strftime("%Y-%m-%d")
            last_l = last.strftime("%Y-%m-%d")
            context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
                created_at__gte=first_f).order_by("-created_at")
        return context


#_________________________________________
# ssh filte
#-----------------------------------------
class CommandsFilter(LoginRequiredMixin, TemplateView):
    template_name = 'common/filtercommandscreate.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if not request.user.is_authenticated:
            return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if not request.user.has_perm('common.can_add_filtercommands'):
            raise PermissionDenied(_('403 Forbidden'))
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(CommandsFilter, self).get_context_data(**kwargs)
        context['server_groups'] = ServerGroup.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context

    def post(self, request):
        if request.is_ajax():
            try:
                if isinstance(request.body, bytes):
                    data = json.loads(request.body.decode())
                else:
                    data = json.loads(request.body)
                if data['action'] == 'create':
                    if not request.user.has_perm('common.can_add_filtercommands'):
                        raise PermissionDenied(_('403 Forbidden'))
                    obj = FilterCommands.objects.create(
                        Profile=data['Profile'], commands=data['commands'], description=data['description'], )
                    obj.save()
                    return JsonResponse({'status': True, 'message': '%s create success!' % (smart_str(data.get('Profile', None)))})
                elif data['action'] == 'update':
                    if not request.user.has_perm('common.can_change_filtercommands'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = FilterCommands.objects.get(
                            id=data.get('id', None))
                        obj.commands = data['commands']
                        obj.Profile = data['Profile']
                        obj.description = data['description']

                        obj.__dict__.update(data)
                        obj.save()
                        return JsonResponse({'status': True, 'message': '%s update success!' % (smart_str(data.get('Profile', None)))})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                elif data['action'] == 'delete':
                    if not request.user.has_perm('common.can_delete_filtercommands'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = FilterCommands.objects.get(
                            id=data.get('id', None))
                        taskname = obj.Profile
                        obj.delete()
                        return JsonResponse({'status': True, 'message': 'Delete Profile %s success!' % (taskname)})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                else:
                    return JsonResponse({'status': False, 'message': 'Illegal action.'})
            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Please input a valid Profile name!'})
            except IntegrityError:
                return JsonResponse({'status': False, 'message': 'Profile name:%s already exist,Please use another name instead!' % (data['Profile'])})
            except KeyError:
                return JsonResponse({'status': False, 'message': "Invalid parameter,Please report it to the adminstrator!"})
            except Exception as e:
                print(traceback.print_exc())
                return JsonResponse({'status': False, 'message': 'Some error happend! Please report it to the adminstrator! Error info:%s' % (smart_str(e))})
        else:
            pass


class CommandsFilterList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = FilterCommands
    template_name = 'common/filtercommandslist.html'
    permission_required = 'common.can_view_filtercommands'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(CommandsFilterList, self).get_context_data(**kwargs)
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class CommandsFilterDetailApi(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'common.can_view_filtercommands'
    raise_exception = True

    def post(self, request):
        if request.is_ajax():
            id = request.POST.get('id', None)
            try:
                data = FilterCommands.objects.get(id=id)
                return JsonResponse({'status': True, 'Profile': data.Profile, 'commands': json.loads(data.commands), 'data': {'Profile': data.Profile,'description': data.description, 'commands': json.loads(data.commands), }})
            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Request object not exist!'})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})

#_________________________________________
# Assign to user
#-----------------------------------------

class AssignProfileToUserList(LoginRequiredMixin, PermissionRequiredMixin, ListView):

    model = AssignFilterToUser
    template_name = 'common/assignprofilelist.html'
    permission_required = 'common.can_view_filtertouser'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(AssignProfileToUserList, self).get_context_data(**kwargs)
        context['users'] = User.objects.all()
        context['profiles'] = FilterCommands.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class AssignProfileToUser(LoginRequiredMixin, TemplateView):
    template_name = 'common/assignprofilecreate.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if not request.user.is_authenticated:
            return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if not request.user.has_perm('common.can_add_filtertouser'):
            raise PermissionDenied(_('403 Forbidden'))
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(AssignProfileToUser, self).get_context_data(**kwargs)
        context['users'] = User.objects.all()
        context['profiles'] = FilterCommands.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context

    def post(self, request):
        if request.is_ajax():
            try:
                if isinstance(request.body, bytes):
                    data = json.loads(request.body.decode())
                else:
                    data = json.loads(request.body)
                if data['action'] == 'create':
                    if not request.user.has_perm('common.can_add_filtertouser'):
                        raise PermissionDenied(_('403 Forbidden'))
                    username = data['user']
                    userdata = User.objects.filter(username__exact=username).get()

                    pattern = re.compile(r'\["')
                    resultat = pattern.findall(data['profile'])
                    if(resultat):
                        profile = data['profile'].split(' ["')[0]

                    profiledata = FilterCommands.objects.filter(Profile__exact=profile).get()
                    obj = AssignFilterToUser()
                    obj.user = userdata
                    obj.filter = profiledata
                    obj.save()

                    return JsonResponse({'status': True, 'message': 'Assignment create success!' })
                elif data['action'] == 'update':
                    if not request.user.has_perm('common.can_change_filtertouser'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = AssignFilterToUser.objects.get(
                            id=data.get('id', None))
                        username = data['user']
                        profile = data['Profile']
                        userdata = User.objects.filter(username__exact=username).get()
                        profiledata = FilterCommands.objects.filter(Profile__exact=profile).get()
                        obj.user = userdata
                        obj.filter = profiledata
                        obj.__dict__.update(data)
                        obj.save()
                        return JsonResponse({'status': True, 'message': 'Assignment update success!'})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                elif data['action'] == 'delete':
                    if not request.user.has_perm('common.can_delete_filtertouser'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = AssignFilterToUser.objects.get(
                            id=data.get('id', None))
                        obj.delete()
                        return JsonResponse({'status': True, 'message': 'Delete assignment success!' })
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                else:
                    return JsonResponse({'status': False, 'message': 'Illegal action.'})
            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Please input a valid group name!'})
            except IntegrityError:
                return JsonResponse({'status': False, 'message': 'This user %s has already this profile' % (data['user'])})
            except KeyError:
                return JsonResponse({'status': False, 'message': "Invalid parameter,Please report it to the adminstrator!"})
            except Exception as e:
                print(traceback.print_exc())
                return JsonResponse({'status': False, 'message': 'Some error happend! Please report it to the adminstrator! Error info:%s' % (smart_str(e))})
        else:
            pass



class AssignProfileToUserDetailApi(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'common.can_change_filtertouser'
    raise_exception = True

    def post(self, request):
        if request.is_ajax():
            id = request.POST.get('id', None)
            try:
                data = AssignFilterToUser.objects.get(id=id)

                return JsonResponse({'status': True, 'user':data.user.username, 'Profile':data.filter.Profile, 'data': {'user':data.user.username, 'Profile':data.filter.Profile} })

            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Request object not exist!'})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})

#_________________________________________
# Time slot
#-----------------------------------------

class TimeSlotList(LoginRequiredMixin, PermissionRequiredMixin, ListView):

    model = TimeSlot
    template_name = 'common/timeslotlist.html'
    permission_required = 'common.can_view_time_slot'
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super(TimeSlotList, self).get_context_data(**kwargs)
        context['users'] = User.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context


class TimeSlotview(LoginRequiredMixin, TemplateView):
    template_name = 'common/timeslotcreate.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if not request.user.is_authenticated:
            return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())
        if not request.user.has_perm('common.can_add_time_slot'):
            raise PermissionDenied(_('403 Forbidden'))
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(TimeSlotview, self).get_context_data(**kwargs)
        context['users'] = User.objects.all()
        first = datetime.today() - timedelta(days=7)
        last = datetime.today() + timedelta(days=1)
        first_f = first.strftime("%Y-%m-%d")
        last_l = last.strftime("%Y-%m-%d")
        context['alerte'] = Notification.objects.filter(created_at__lte=last_l).filter(
            created_at__gte=first_f).order_by("-created_at")
        return context

    def post(self, request):
        if request.is_ajax():
            try:
                if isinstance(request.body, bytes):
                    data = json.loads(request.body.decode())
                else:
                    data = json.loads(request.body)
                if data['action'] == 'create':
                    if not request.user.has_perm('common.can_add_time_slot'):
                        raise PermissionDenied(_('403 Forbidden'))
                    username = data['user']
                    userdata = User.objects.filter(username__exact=username).get()

                    obj = TimeSlot(name = data['name'],description = data['description'],
                                   start_at = data['start at'], end_at = data['end at'],close=data['close'])
                    obj.user = userdata
                    if 'Weekdays' in data:
                        weekdays = data['Weekdays']
                        days = []
                        for d in weekdays:
                            if d == "Monday" : days.append(0)
                            elif d == "Tuesday" : days.append(1)
                            elif d == "Wednesday" : days.append(2)
                            elif d == "Thursday" : days.append(3)
                            elif d == "Friday" : days.append(4)
                            elif d == "Saturday" : days.append(5)
                            elif d == "Sunday" : days.append(6)
                        obj.weekdays = days
                    #obj.weekdays = ''
                    obj.save()

                    return JsonResponse({'status': True, 'message': 'Time slot create success!' })
                elif data['action'] == 'update':
                    if not request.user.has_perm('common.can_change_time_slot'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = TimeSlot.objects.get(
                            id=data.get('id', None))
                        username = data['user']
                        userdata = User.objects.filter(username__exact=username).get()
                        obj.user = userdata
                        obj.description =data['description']
                        obj.name = data['name']
                        obj.start_at = data['start at']
                        obj.end_at = data['end at']
                        obj.close = data['close']
                        if 'Weekdays' in data:

                            weekdays = data['Weekdays']
                            days = []
                            for d in weekdays:
                                if d == "Monday":
                                    days.append(0)
                                elif d == "Tuesday":
                                    days.append(1)
                                elif d == "Wednesday":
                                    days.append(2)
                                elif d == "Thursday":
                                    days.append(3)
                                elif d == "Friday":
                                    days.append(4)
                                elif d == "Saturday":
                                    days.append(5)
                                elif d == "Sunday":
                                    days.append(6)
                            obj.weekdays = days
                        else:
                            obj.weekdays = ''
                        obj.__dict__.update(data)
                        obj.save()
                        return JsonResponse({'status': True, 'message': 'Time slot update success!'})
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                elif data['action'] == 'delete':
                    if not request.user.has_perm('common.can_delete_time_slot'):
                        raise PermissionDenied(_('403 Forbidden'))
                    try:
                        obj = TimeSlot.objects.get(
                            id=data.get('id', None))
                        obj.delete()
                        return JsonResponse({'status': True, 'message': 'Delete Time slot success!' })
                    except ObjectDoesNotExist:
                        return JsonResponse({'status': False, 'message': 'Request object not exist!'})
                else:
                    return JsonResponse({'status': False, 'message': 'Illegal action.'})
            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Please input a valid name!'})
            except IntegrityError:
                return JsonResponse({'status': False, 'message': 'This Time slot %s has already this user' % (data['user'])})
            except KeyError:
                return JsonResponse({'status': False, 'message': "Invalid parameter,Please report it to the adminstrator!"})
            except Exception as e:
                print(traceback.print_exc())
                return JsonResponse({'status': False, 'message': 'Some error happend! Please report it to the adminstrator! Error info:%s' % (smart_str(e))})
        else:
            pass



class TimeSlotDetailApi(LoginRequiredMixin, PermissionRequiredMixin, View):
    permission_required = 'common.can_change_time_slot'
    raise_exception = True

    def post(self, request):
        if request.is_ajax():
            id = request.POST.get('id', None)
            try:
                data = TimeSlot.objects.get(id=id)
                start = data.start_at.strftime("%Y/%m/%d %H:%M")
                end = data.end_at.strftime("%Y/%m/%d %H:%M")
                week= ["Saturday", "Monday"]

                days = []
                for d in data.weekdays:
                    if d == "0": days.append("Monday")
                    elif d == "1": days.append("Tuesday")
                    elif d == "2":days.append("Wednesday")
                    elif d == "3": days.append("Thursday")
                    elif d == "4":days.append("Friday")
                    elif d == "5":days.append("Saturday")
                    elif d == "6": days.append("Sunday")


                return JsonResponse({'status': True, 'user':data.user.username,  'data': {'user':data.user.username, 'name':data.name, 'description':data.description,
                                            'start at':start, 'end at':end, 'close':data.close, 'Weekdays': days} })

            except ObjectDoesNotExist:
                return JsonResponse({'status': False, 'message': 'Request object not exist!'})
        else:
            return JsonResponse({'status': False, 'message': 'Method not allowed!'})

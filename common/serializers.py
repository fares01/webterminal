from rest_framework import serializers
from common.models import ServerInfor, ServerGroup, Credential, CommandsSequence
from .models import AssignFilterToUser, FilterCommands
from django.contrib.auth.models import User

class ServerInforSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ServerInfor
        fields = '__all__'


class ServerGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ServerGroup
        fields = '__all__'


class CredentialSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Credential
        fields = '__all__'


class CommandsSequenceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CommandsSequence
        fields = '__all__'

#___________________________
# SSh filter
#_____________________
class AssignFilterToUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AssignFilterToUser
        fields = '__all__'


class FilterCommandsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FilterCommands
        fields = '__all__'

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from permission.models import Role, Permission

# Register your models here.

class RoleAdmin(admin.ModelAdmin):
    filter_horizontal = ('permissions','groups')

class PermissionAdmin(admin.ModelAdmin):
    filter_horizontal = ('permissions',)

admin.site.register(Role,RoleAdmin)


admin.site.register(Permission,PermissionAdmin)